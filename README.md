# README #

### Indehold? ###
Oplæg om Adapter pattern til faget I4SWD på Aarhus Universitet.

### Hvordan laver jeg min egen pdf? ###

* Installer TeXLive/MikTex
* Installer evtl Perl (til at køre latexmk)
* Kør "latexmk -pdf -silent -pdflatex="pdflatex -synctex=1 %O %S" "master"" i doc mappen

### Kilder med inspiration ###

http://www.dofactory.com/net/adapter-design-pattern
http://www.blackwasp.co.uk/Adapter.aspx
http://www.oodesign.com/adapter-pattern.html
http://msdn.microsoft.com/en-us/library/orm-9780596527730-01-04.aspx
http://stackoverflow.com/questions/350404/how-do-the-proxy-decorator-adapter-and-bridge-patterns-differ
http://en.wikipedia.org/wiki/Adapter_pattern
http://java.dzone.com/articles/design-patterns-uncovered-0
http://www.tutorialspoint.com/design_pattern/adapter_pattern.htm